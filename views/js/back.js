$(document).ready(function () {

    let checkbox = document.getElementById("product_option_form_isOrder");
    let pikerContainer = document.getElementById("product_option_form_dateExpiration").parentElement.parentElement.parentElement;
    if (checkbox.checked == true) {
        pikerContainer.style.display = "";
    } else {
        pikerContainer.style.display = "none";
    }

    document.getElementById("product_option_form_isOrder").onclick = function () {
        let pikerContainer = document.getElementById("product_option_form_dateExpiration").parentElement.parentElement.parentElement;
        if (this.checked == true) {
            pikerContainer.style.display = "";
        } else {
            pikerContainer.style.display = "none";
        }
    }

    $("#form_step1").parent().css('flex-basis','revert');  
});