<?php

/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

declare(strict_types=1);

use Doctrine\DBAL\Types\TextType;
use PhpParser\Node\Scalar\MagicConst\Dir;
use PrestaShop\PrestaShop\Adapter\SymfonyContainer;
use Prestashop\PrestaShop\Core\Product\ProductAdminDrawer;
use PrestaShop\Module\Preorder\Entity\PreorderProductAttribute;
use PrestaShop\Module\Preorder\Entity\PreorderProductName;
use PrestaShop\Module\Preorder\Install\Installer;
use PrestaShop\Module\Preorder\Forms\Types\PreorderFormType;
use PrestaShop\Module\Preorder\Forms\Types\PreorderFormProductNameType;
use PrestaShop\Module\Preorder\Forms\Types\ProductInformationType;
use PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductLazyArray;
use Symfony\Component\Form\Extension\Core\Type\FormType;

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__ . DIRECTORY_SEPARATOR .  'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require_once _PS_CORE_DIR_ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Core' . DIRECTORY_SEPARATOR . 'Product' . DIRECTORY_SEPARATOR . 'ProductAdminDrawer.php';


class Preorder extends Module
{
    protected $config_form = false;

    const CATEGORY_ID = 140;
    const FEATURE_ID = 26;
    const FEATURE_VALUE = 904;

    public function __construct()
    {
        $this->name = 'preorder';
        $this->tab = 'administration';
        $this->version = '1.7.8';
        $this->author = 'Valentyn';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Preorder');
        $this->description = $this->l('Preorder module this is ');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('PREORDER_LIVE_MODE', false);

        $installer = new Installer;
        $installer->install();

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('actionAdminControllerSetMedia') &&
            $this->registerHook('displayAdminProductsOptionsStepTop') &&
            $this->registerHook('actionProductUpdate') &&
            $this->registerHook('displayProductPriceBlock') &&
            $this->registerHook('actionFrontControllerInitAfter') &&
            $this->registerHook('actionAdminControllerInitAfter') &&
            $this->registerHook('displayProductPageDrawer');
    }


    public function uninstall()
    {
        Configuration::deleteByName('PREORDER_LIVE_MODE');
        $installer = new Installer();
        $installer->uninstall();

        return parent::uninstall();
    }

    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
    }

    // cleare sessions for front office
    public function hookActionFrontControllerInitAfter()
    {
        session_start();
        if(isset($_SESSION['product_id_container'])){
            unset($_SESSION['product_id_container']);
        }
    }   

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookDisplayBackOfficeHeader()
    {        
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');        
    }

    public function hookDisplayProductPageDrawer($params){
        $productId = $params['product']->id;
        $productName  = $params['product']->name;
        $textProductName = "";
        if(is_array($productName) && sizeof($productName) > 0 ){
            $textProductName  = array_shift($productName);
        }
        $entityManager = SymfonyContainer::getInstance()->get('doctrine.orm.entity_manager');
        $preorderProductAttributeRepo = $entityManager->getRepository(PreorderProductAttribute::class);
        $preorderProductAttribute  = $preorderProductAttributeRepo->findOneBy(['productId' => $productId]);
        $preorder = ( isset($preorderProductAttribute) ? true : false );
    
        
        $preorderProductName  = new PreorderProductName();
        $preorderProductName->setName($textProductName); 
        

        $translator = SymfonyContainer::getInstance()->get('translator');
        $legacyContext = SymfonyContainer::getInstance()->get('prestashop.adapter.legacy.context')->getLanguages();

        $form = SymfonyContainer::getInstance()->get('form.factory')->createBuilder(FormType::class)
        ->add('step1', ProductInformationType::class,['interfases' => [
                    't' => $translator,
                    'l' => $legacyContext,
                    'value' => $textProductName,
                    'preorder' => $preorder
                ],
            'label' => false,
            ])->getForm();
       
         return  [new ProductAdminDrawer(['id' => $form->createView()])];
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    // 
    public function hookDisplayAdminProductsOptionsStepTop($params)
    {
        $productId = $params['id_product'];

        $formFactory = SymfonyContainer::getInstance()->get('form.factory');
        $twig = SymfonyContainer::getInstance()->get('twig');

        $entityManager = SymfonyContainer::getInstance()->get('doctrine.orm.entity_manager');
        $preorderProductAttributeRepo = $entityManager->getRepository(PreorderProductAttribute::class);
        $preorderProductAttribute  = $preorderProductAttributeRepo->findOneBy(['productId' => $productId]);
        $form = $formFactory->createNamedBuilder('product_option_form', PreorderFormType::class, $preorderProductAttribute)->getForm();

        $template = '@Modules/preorder/views/templates/admin/seo_special_field.html.twig';

        return $twig->render($template, [
            'product_option_form' => $form->createView(),
            'id_product' => $productId
        ]);
    }

    public function hookActionProductUpdate($params)
    {
        if(isset($_SESSION['hookActionProductUpdate'])){
            unset($_SESSION['hookActionProductUpdate']);
            return;
        }
        $productId = $params['id_product'];
        $product = $params['product'];

        $productOptionForm = Tools::getValue("product_option_form");
        if ($productOptionForm) {
            // search our object into table
            $entityManager = SymfonyContainer::getInstance()->get('doctrine.orm.entity_manager');
            $preorderProductAttributeRepo = $entityManager->getRepository(PreorderProductAttribute::class);
            $preorderProductAttribute  = $preorderProductAttributeRepo->findOneBy(['productId' => $productId]);

            $isOrder = (isset($productOptionForm['isOrder']) ? intval($productOptionForm['isOrder']) : 0);
            // update record
            if (isset($preorderProductAttribute)) {
                // update
                if ($isOrder === 1) {
                    $dateExpiration = "";
                    if (!isset($productOptionForm['dateExpiration'])) {
                        throw new Exception("not data");
                    }
                    $dateExpiration = $productOptionForm['dateExpiration'];
                    $preorderProductAttribute->setIsOrder(true)
                        ->setDateAdd(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')))
                        ->setDateExpiration(\DateTime::createFromFormat('Y-m-d', $dateExpiration))
                        ->setProductId($productId);
                    $entityManager->persist($preorderProductAttribute);
                    $entityManager->flush();
                } else {
                    // delete  records from the preorderProductAttribute
                    // we clean everything and leave
                    $entityManager->remove($preorderProductAttribute);
                    $entityManager->flush();
                    // delete category from the product 
                    $this->deleteCategoryFromProducts($product,Preorder::CATEGORY_ID);
                    // delete feature from the product
                    $this->deleteFeatureFromProduct($productId,Preorder::FEATURE_ID);
                    return;
                }
            } else {
                // create record if isOrder === 1
                if ($isOrder === 1) {
                    $dateExpiration = "";
                    if (!isset($productOptionForm['dateExpiration'])) {
                        throw new Exception("not data");
                    }
                    $dateExpiration = $productOptionForm['dateExpiration'];
                    $preorderProductAttribute = new PreorderProductAttribute;
                    $preorderProductAttribute->setIsOrder(true)
                        ->setDateAdd(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')))
                        ->setDateExpiration(\DateTime::createFromFormat('Y-m-d', $dateExpiration))
                        ->setProductId($productId);
                    $entityManager->persist($preorderProductAttribute);
                    $entityManager->flush();
                }
            }
            //this code will only be executed if we receive a form with our pre-order
           $this->addCategoryToProduct($product,[Preorder::CATEGORY_ID]);
           $this->addFeaturesToProduct($product,Preorder::FEATURE_ID,Preorder::FEATURE_VALUE);
        }
        // to avoid multiple triggering
        if(!isset($_SESSION['hookActionProductUpdate'])){
            $_SESSION['hookActionProductUpdate'] = 'preorder';
        }

        // fix bug , after installations module  and update product , product is not active
        Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'product_shop` SET active=1 WHERE `id_product` = ' . $productId);
    }

    public function hookDisplayProductPriceBlock($params)
    {
        if( isset($params['product']) && isset($params['type'])){
            $productId = $params['product']->getId();
            if(( $params['type'] === 'before_price' || $params['type'] === 'old_price' || $params['type'] === 'price')){
                // init seshions conteiner
                if(!isset($_SESSION['product_id_container'])){
                    $_SESSION['product_id_container'] = [];
                }
                if(!isset($_SESSION['product_id_container'][$productId])){
                    $_SESSION['product_id_container'][$productId] = $params['type'];
                    return $this->getPreorderAttribute($productId);
                }
            }            
        }
        return false;
    }

    public function getPreorderAttribute($productId){
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $preorderProductAttributeRepo = $entityManager->getRepository(PreorderProductAttribute::class);
        $preorderProductAttribute  = $preorderProductAttributeRepo->findOneBy(['productId' => $productId]);
        if($preorderProductAttribute !== null){
            $dateExpiration = $preorderProductAttribute->getDateExpiration()->format("Y-m-d");
            $isOrder = intval($preorderProductAttribute->getIsOrder());
            if($isOrder === 1){
                return "<h5>" .  "preorder" . " " . $dateExpiration . "</h5>" . "<br>";
            }
        }
    }

    public function addCategoryToProduct(Product $product,Array $categories = []){
        //first validation if categori in exists in product
        $curentCategories = $product->getCarriers();
        $result = array_diff($curentCategories, $categories);
        if(!empty($result)){
            $product->updateCategories($result,true);
        }
    }
    public function addFeaturesToProduct(Product $product,$id_feature, $id_value){
        // $productId = $product->id;
        // $fuateres = Product::getFeaturesStatic($productId);
        // $test = false;
        // if(empty($fuateres)){
        //     $product->addFeaturesToDB($id_feature, $id_value, $cust = 0);
        // }else{
        //     foreach($fuateres as  $v){
        //         if($v['id_feature'] == $id_feature && $v['id_feature_value'] == $id_value){
        //             $test = true;
        //             break;
        //         }
        //     }
        // }

        // if($test !== true){
        //     $product->addFeaturesToDB($id_feature, $id_value, $cust = 1);
        // }
    }

    public function deleteCategoryFromProducts(Product $product ,$id_category){
        $product->deleteCategory($id_category);
    }

    public function deleteFeatureFromProduct($productId,$featureId){        
        // $all_shops = Context::getContext()->shop->getContext() == Shop::CONTEXT_ALL ? true : false;

        // // List products features
        // $features = Db::getInstance()->executeS(
        //     '
        //     SELECT p.*, f.*
        //     FROM `' . _DB_PREFIX_ . 'feature_product` as p
        //     LEFT JOIN `' . _DB_PREFIX_ . 'feature_value` as f ON (f.`id_feature_value` = p.`id_feature_value`)
        //     ' . (!$all_shops ? 'LEFT JOIN `' . _DB_PREFIX_ . 'feature_shop` fs ON (f.`id_feature` = fs.`id_feature`)' : null) . '
        //     WHERE `id_product` = ' . (int) $productId
        //         . (!$all_shops ? ' AND fs.`id_shop` = ' . (int) Context::getContext()->shop->id : '')
        // );

        // foreach ($features as $tab) {
        //     // Delete product custom features
        //     if ($tab['custom']) {
        //         Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'feature_value` WHERE `id_feature_value` = ' . (int) $tab['id_feature_value']);
        //         Db::getInstance()->execute('DELETE FROM `' . _DB_PREFIX_ . 'feature_value_lang` WHERE `id_feature_value` = ' . (int) $tab['id_feature_value']);
        //     }
        // }
        // // Delete product features
        // $result = Db::getInstance()->execute('
        //     DELETE `' . _DB_PREFIX_ . 'feature_product` FROM `' . _DB_PREFIX_ . 'feature_product`
        //     WHERE `id_product` = ' . (int) $productId . (!$all_shops ? '
        //         AND `id_feature` IN (
        //             SELECT `id_feature`
        //             FROM `' . _DB_PREFIX_ . 'feature_shop`
        //             WHERE `id_shop` = ' . (int) Context::getContext()->shop->id . '
        //         )' : ''));

        // SpecificPriceRule::applyAllRules([(int) $productId]);

        // return $result;
    }
}
