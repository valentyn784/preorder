<?php

class ProductController extends ProductControllerCore
{

    public function initContent()
    {
        $productId = $this->product->id;
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $preorderProductAttributeRepo = $entityManager->getRepository(PreorderProductAttribute::class);
        $preorderProductAttribute  = $preorderProductAttributeRepo->findOneBy(['productId' => $productId]);
        $preorder = (isset($preorderProductAttribute) ? 1 : 0);

        $this->context->smarty->assign([
            '_preorder' => $preorder,
        ]);

        parent::initContent();
    }
}
