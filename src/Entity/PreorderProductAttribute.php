<?php

namespace PrestaShop\Module\Preorder\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ps_preorder_product_custom_attribute")
 * @ORM\Entity()
 */
class PreorderProductAttribute{
     /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="product_id", type="integer")
     */
    private $productId;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="date_expiration", type="datetime")
     */
    private $dateExpiration;

    /**
     *@var bool
     *
     * @ORM\Column(name="is_order",  type="boolean", nullable=false)
     */
    private $isOrder;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;  

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     *
     * @return PreorderCustomProductAttribute
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateExpiration()
    {
        return $this->dateExpiration;
    }

    /**
     *
     *
     * @return PreorderCustomProductAttribute
     */
    public function setDateExpiration(\DateTimeInterface $dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsOrder()
    {
        return $this->isOrder;
    }

    /**
     * 
     *
     * @return PreorderCustomProductAttribute
     */
    public function setIsOrder(bool $isOrder)
    {
        $this->isOrder = $isOrder;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * 
     *
     * @return PreorderCustomProductAttribute
     */
    public function setDateAdd(\DateTimeInterface $dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }
    

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'product_id' => $this->getProductId(),
            'date_expiration' => $this->getDateExpiration(),
            'is_order' => $this->getIsOrder(),
            'date_add' => $this->getDateAdd(),
            ];
    }
}