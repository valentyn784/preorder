<?php

namespace PrestaShop\Module\Preorder\Entity;

class PreorderProductName{

    private $name;

     /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     *
     * @return PreorderProductName
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}