<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

declare(strict_types=1);

namespace PrestaShop\Module\Preorder\Install;

final class Installer
{
    /**
     * Module's installation entry point.
     *
     * 
     *
     * @return bool
     */
    public function install(): bool
    {
        $sql = array();
        $sql[] = "CREATE TABLE IF NOT EXISTS " . _DB_PREFIX_ .  "preorder_product_custom_attribute
        (
            id INT(11) UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
            product_id INT(11)  UNSIGNED NOT NULL,
            date_expiration datetime NOT NULL,
            is_order boolean DEFAULT NULL,
            date_add datetime NOT NULL,
            PRIMARY KEY(id)
        )  DEFAULT CHARSET = utf8;";

        foreach ($sql as $query) {
            if (\Db::getInstance()->execute($query) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * 
     *
     * @return bool
     */
    public function uninstall(): bool
    {
        $sql[] = "DROP TABLE  `" . _DB_PREFIX_ . "preorder_product_custom_attribute`";
        foreach ($sql as $query) {
            if (\Db::getInstance()->execute($query) == false) {
                return false;
            }
        }
        return true;
    }
}
