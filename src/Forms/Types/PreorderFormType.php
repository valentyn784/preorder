<?php 

namespace PrestaShop\Module\Preorder\Forms\Types;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use PrestaShopBundle\Form\Admin\Type\DatePickerType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;

class PreorderFormType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $obj = ( isset($options['data']) ? $options['data'] : null );
        $dateExpiration = ( !isset($obj) ? "" : $obj->getDateExpiration()->format('Y-m-d') );
        
        $builder
            ->add('isOrder', CheckboxType::class , [
                'label' => 'Is it preorder?',
            ])
            ->add('dateExpiration', DatePickerType::class, [                
                //'label' => "Choose Data",
                'attr' => [
                    'class' => 'date',
                    'placeholder' => 'YYYY-MM-DD',
                    'value' => $dateExpiration,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}