<?php

namespace PrestaShop\Module\Preorder\Forms\Types;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use PrestaShopBundle\Form\Admin\Type\TranslateType;
use PrestaShop\PrestaShop\Adapter\Configuration;
use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Component\Form\FormBuilderInterface;
use PrestaShopBundle\Form\Admin\Type\CommonAbstractType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductInformationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (isset($options['interfases'])) {

            $t = $options['interfases']['t'];
            $l = $options['interfases']['l'];
            $value  = $options['interfases']['value'];
            $preorder = $options['interfases']['preorder'];

            $builder->add('name', TranslateType::class, [
                'type' => FormType\TextType::class,
                'options' => [
                    'constraints' => [
                        new Assert\Regex([
                            'pattern' => '/[<>;=#{}]/',
                            'match' => false,
                        ]),
                        new Assert\NotBlank(),
                        new Assert\Length(['min' => 3, 'max' => 128]),
                    ],
                    'attr' => [
                        'placeholder' => $t->trans('Enter your product name', [], 'Admin.Catalog.Help'),
                        'class' => 'edit js-edit serp-default-title',
                        'value' => $value
                    ],
                ],
                'locales' => $l,
                'hideTabs' => false,
                'label' => ( $preorder === true ? 'Preorder' : false),
                'label_attr' => ['class' => 'preorder-definithion', 'for'=>"form[step1][name][1]"],
            ]);
        }
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'interfases' => []
        ]);
    }
}
