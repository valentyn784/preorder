<?php
namespace PrestaShop\Module\Preorder\Forms\Handler;

use PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataHandler\FormDataHandlerInterface;

final class PreorderFormDataHandler implements FormDataHandlerInterface
{
    /**
     * Create object from form data.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        $contactObjectModel = new Contact();
        // add data to object model
        // ...
        $contactObjectModel->save();

        return $contactObjectModel->id;
    }

    /**
     * Update object with form data.
     *
     * @param int $id
     * @param array $data
     */
    public function update($id, array $data)
    {
        $contactObjectModel = new Contact(id);
        // update data to object model
        // ...
        $contactObjectModel->update();
    }
}