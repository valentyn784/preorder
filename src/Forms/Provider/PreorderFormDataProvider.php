<?php

namespace PrestaShop\Module\Preorder\Forms\Provider;

use  PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataProvider\FormDataProviderInterface;
use Exception;
use Product;

final class PreorderFormDataProvider implements FormDataProviderInterface
{
    /**
     * Get form data for given object with given id.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getData($productId)
    {
        $productObjectModel = new Product($productId);
        
        // check that the element exists in db
        if (empty($productObjectModel->id)) {
            throw new Exception('Object not found');
        }

        return [
            'title' => $productObjectModel->name,
        ];
    }

    /**
     * Get default form data.
     *
     * @return mixed
     */
    public function getDefaultData()
    {
        return [
            'title' => 'service',
        ];
    }
}